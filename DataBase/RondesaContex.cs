﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataBase.Mapping;
using System.Data.Entity;
using Entities;

namespace DataBase
{
   public class RondesaContex: DbContext
    {
       public RondesaContex()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RondesaContex>());
        }
        public DbSet<Producto> Produsctos { get; set; }
        public DbSet<Proveedor> Proveedores { get; set; }
        public DbSet<TipoProducto> TipoProductos { get; set; }
        public DbSet<Bienes> Bienes { get; set; }
        public DbSet<Agencia> Agencias { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Depreciacion> Depreciaciones { get; set; }
        public DbSet<Almacen> Almacenes { get; set; }
        public DbSet<Movimientos> Movimientos { get; set; }
      

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProductoMapping());
            modelBuilder.Configurations.Add(new ProveedorMap());
            modelBuilder.Configurations.Add(new TipoProductoMap());
            modelBuilder.Configurations.Add(new BienesMap());
            modelBuilder.Configurations.Add(new AgenciaMap());
            modelBuilder.Configurations.Add(new AreaMap());
            modelBuilder.Configurations.Add(new DepreciacionMap());
            modelBuilder.Configurations.Add(new AlmacenMap());
            modelBuilder.Configurations.Add(new MovimientosMap());
         
        }
    }
}
