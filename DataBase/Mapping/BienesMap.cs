﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class BienesMap:EntityTypeConfiguration<Bienes>
    {
       public BienesMap()
       {
           this.HasKey(p => p.Idbienes);
           this.Property(p => p.Idbienes).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Descripcion).HasMaxLength(100).IsRequired();
           this.Property(p => p.Grupo).IsRequired();
           this.Property(p => p.CodigoGrupo).IsRequired();
           this.Property(p => p.Estado).HasMaxLength(50).IsRequired();
           this.Property(p => p.NurFactura).IsRequired();
           this.Property(p => p.FechaEmision).IsRequired();
           this.Property(p => p.ImporteSinIGV).IsRequired();
           this.Property(p => p.PrecioInicial).IsRequired();
           this.Property(p => p.MontoActual).IsRequired();
           this.Property(p => p.NurAños).IsRequired();

           this.HasRequired(p => p.Producto).WithMany().HasForeignKey(p => p.IdProductoFk).WillCascadeOnDelete(true);
           this.HasRequired(p => p.Agencia).WithMany().HasForeignKey(p => p.IdAgencia).WillCascadeOnDelete(true);
           this.HasRequired(p => p.Depreciacion).WithMany().HasForeignKey(p => p.IdDepreciacionFk).WillCascadeOnDelete(true);
           this.ToTable("Bienes");
       }
    }
}
