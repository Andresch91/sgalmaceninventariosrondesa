﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class ProveedorMap:EntityTypeConfiguration<Proveedor>
    {
       public ProveedorMap()
       {
           this.HasKey(p => p.IdProveedor);
           this.Property(p => p.IdProveedor).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Representante).IsRequired().HasMaxLength(100);
           this.Property(p => p.Correo).IsRequired().HasMaxLength(50);
           this.Property(p => p.Telefono).IsRequired();
           this.Property(p => p.RazonSocial).IsRequired().HasMaxLength(100);

           this.ToTable("Proveedor");
       }

    }
}
