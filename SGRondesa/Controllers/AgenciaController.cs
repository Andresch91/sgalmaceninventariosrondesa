﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entities;
using Repository;
using Interfaces;
using DataBase;
using Validator;


namespace SGRondesa.Controllers
{
    public class AgenciaController : Controller
    {
        private InterfaceAgencia repo;

          public AgenciaController (AgenciaRepository repo)
        {
            this.repo = repo;            
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarAgencia");
        }

        [HttpPost]
        public ActionResult Create(Agencia agencia)
        {
           
            if (ModelState.IsValid)
            {
                repo.Store(agencia);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }


            return View("RegistrarAgencia", agencia);

        }

    }
}
