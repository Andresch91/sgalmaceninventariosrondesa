﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Importamos
using Interfaces;
using Entities;
using Validator.TipoProductoValidators;

namespace SGRondesa.Controllers
{
    public class TipoProductoController : Controller
    {
       private InterfaceTipoProducto repository;
        private TipoProductoValidators validator;

        public TipoProductoController(InterfaceTipoProducto repository, TipoProductoValidators validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.AllTipoProducto(query);
            return View("ListaTipoProductos", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarTipoProducto");
        }

        [HttpPost]
        public ActionResult Create(TipoProducto tipoProducto)
        {

            if (ModelState.IsValid)
            {
                repository.Store(tipoProducto);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }

            return View("RegistrarTipoProducto", tipoProducto);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            return View("EditarTipoProducto", data);
        }

        [HttpPost]
        public ActionResult Edit(TipoProducto tipoProducto)
        {
            repository.Update(tipoProducto);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }
    }
}
