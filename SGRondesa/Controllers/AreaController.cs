﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entities;
using Repository;
using Interfaces;
using DataBase;
using Validator.AreaValidators;

namespace SGRondesa.Controllers
{
    public class AreaController : Controller
    {
        private InterfaceArea repo;
        private InterfaceAgencia repoAge;
        private AreaValidators validator;

        public AreaController(AreaRepository repo, AgenciaRepository repoAge, AreaValidators validator)
        {
            this.repo = repo;
            this.repoAge = repoAge;
            this.validator = validator;
        }
    }
}
