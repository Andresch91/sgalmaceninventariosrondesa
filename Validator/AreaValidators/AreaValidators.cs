﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;

namespace Validator.AreaValidators
{
   public class AreaValidators
    {
       public static bool Pass(Area area)
       {
           if (string.IsNullOrEmpty(area.IdAgenciaFk.ToString())
               || string.IsNullOrWhiteSpace(area.IdAgenciaFk.ToString()))
               return false;
           return true;
       }
    }
}
