﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;

namespace Interfaces
{
   public interface InterfaceArea
    {
        List<Area> All();
        List<Area> AllArea(string criterio);
        void Store(Area area);
        Area Find(int id);
        void Update(Area Area);
        void Delete(int id);
    }
}
