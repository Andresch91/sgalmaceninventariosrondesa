﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class Proveedor
    {
        public Int32 IdProveedor { get; set; }
        public string Representante { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string RazonSocial { get; set; }
    }
}
