﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class Area
    {
        public Int32 IdArea { get; set; }
        public string Nombre { get; set; }
        public string CodigoArea { get; set; }

        public Int32 IdAgenciaFk { get; set; }
        public Agencia Agencia { get; set; }
    }
}
