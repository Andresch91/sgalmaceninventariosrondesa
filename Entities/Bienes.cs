﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class Bienes
    {
        public Int32 Idbienes { get; set; }
        public string Descripcion { get; set; }
        public Int32 Grupo { get; set; }
        public Int32 CodigoGrupo { get; set; }
        public string Estado { get; set; }
        public string NurFactura { get; set; }
        public DateTime FechaEmision { get; set; }
        public decimal ImporteSinIGV { get; set; }
        public decimal PrecioInicial { get; set; }
        public decimal MontoActual { get; set; }
        public Int32 NurAños { get; set; }

        public Int32 IdProductoFk { get; set; }
        public Producto Producto { get; set; }

        public Int32 IdAgencia { get; set; }
        public Agencia Agencia { get; set; }

        public Int32 IdDepreciacionFk { get; set; }
        public Depreciacion Depreciacion { get; set; }


    }
}
