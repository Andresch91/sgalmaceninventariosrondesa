﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataBase;
using Interfaces;
using Entities;
using System.Data;

namespace Repository
{
   public class AgenciaRepository: MasterRepository, InterfaceAgencia
    {

        public List<Agencia> All()
        {
            var query = from a in Context.Agencias
                        select a;
            return query.ToList();
        }

        public List<Agencia> AllAgencia(string criterio)
        {
            var query = from a in Context.Agencias select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.Nombre.ToUpper().Contains(criterio.ToUpper()));

            }
            return query.ToList();
        }

        public void Store(Agencia agencia)
        {
            Context.Agencias.Add(agencia);
            Context.SaveChanges();
        }

        public Agencia Find(int id)
        {
            var result = from p in Context.Agencias
                         where p.IdAgencia == id
                         select p;
            return result.FirstOrDefault();
        }

        public void Update(Agencia agencia)
        {
            var resul = (from p in Context.Agencias
                         where p.IdAgencia == agencia.IdAgencia
                         select p).First();
            resul.Nombre = agencia.Nombre;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in Context.Agencias where p.IdAgencia == id select p).First();
            Context.Agencias.Remove(result);
            Context.SaveChanges();
        }
    }
}
