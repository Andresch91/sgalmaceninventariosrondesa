﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Importamos
using DataBase;
using Interfaces;
using Entities;
using System.Data;

namespace Repository
{
   public class ProveedorRepository:InterfaceProveedor
    {
       RondesaContex entities;
       public ProveedorRepository(RondesaContex entities)
        {
            this.entities = entities;
        }



        public List<Proveedor> AllCriteriosProveedor()
        {
            var result = from p in entities.Proveedores select p;
            return result.ToList();
        }

        public void Store(Proveedor proveedor)
        {
            entities.Proveedores.Add(proveedor);
            entities.SaveChanges();
        }

        public Proveedor Find(int id)
        {
            var result = from p in entities.Proveedores where p.IdProveedor == id select p;
            return result.FirstOrDefault();
        }

        public void Update(Proveedor proveedor)
        {
            var result = (from p in entities.Proveedores where p.IdProveedor == proveedor.IdProveedor select p).First();

            //result.Presencia = criterioevaluacion.Presencia;
            //result.DominioTema = criterioevaluacion.DominioTema;

            entities.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in entities.Proveedores where p.IdProveedor == id select p).First();
            entities.Proveedores.Remove(result);
            entities.SaveChanges();
        }

        public List<Proveedor> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.Proveedores select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Representante.Contains(query));

            return dbQuery.ToList();
        }
    }
}
