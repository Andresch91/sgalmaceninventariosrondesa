﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Interfaces;
using Entities;
using DataBase;

namespace Repository
{
  public class TipoProductoRepository:MasterRepository,InterfaceTipoProducto
    {

        public List<TipoProducto> All()
        {
            var query = from a in Context.TipoProductos
                        select a;
            return query.ToList();
        }

        public List<TipoProducto> AllTipoProducto(string criterio)
        {
            var query = from a in Context.TipoProductos select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.Descripcion.ToUpper().Contains(criterio.ToUpper()));
            }
            return query.ToList();
        }

        public void Store(TipoProducto tipoProducto)
        {
            Context.TipoProductos.Add(tipoProducto);
            Context.SaveChanges();
        }

        public TipoProducto Find(int id)
        {
            var result = from p in Context.TipoProductos
                         where p.IdTipoProducto == id
                         select p;
            return result.FirstOrDefault();
        }

        public void Update(TipoProducto tipoProducto)
        {
            var resul = (from p in Context.TipoProductos
                         where p.IdTipoProducto == tipoProducto.IdTipoProducto
                         select p).First();
           // resul.Nombre = Area.Nombre;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in Context.TipoProductos where p.IdTipoProducto == id select p).First();
            Context.TipoProductos.Remove(result);
            Context.SaveChanges();
        }
    }
}
